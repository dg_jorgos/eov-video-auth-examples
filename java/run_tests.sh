#!/bin/sh -e

javac -cp 'dep/commons-codec-1.10.jar:dep/commons-logging-1.2.jar:dep/hamcrest-core-1.3.jar:dep/httpclient-4.5.3.jar:dep/httpcore-4.4.6.jar:dep/junit-4.12.jar' Authenticator.java AuthenticatorTests.java
jar cf AuthenticatorTests.jar -C . .
java -cp 'AuthenticatorTests.jar:dep/commons-codec-1.10.jar:dep/commons-logging-1.2.jar:dep/hamcrest-core-1.3.jar:dep/httpclient-4.5.3.jar:dep/httpcore-4.4.6.jar:dep/junit-4.12.jar' org.junit.runner.JUnitCore tests
