import org.junit.Test;

public class AuthenticatorTests
{
    @Test
    public void test() throws Exception
    {
        String uid = "uid1";
        String host = "prod.rtcvid.net";
        String projectName = "project_name";
        String preSharedSecret = "preSharedSecret";
        String algorithm = "sha1";
        String timestamp = "1490777948";
        String expectedKey = "iJSJZ6XycmW8C%2FsJ3koL%2BeXC3FU%3D";

        String key = Authenticator.createAuthenticationString(preSharedSecret, algorithm, uid, timestamp);

        org.junit.Assert.assertEquals(key, expectedKey);
    }
}
