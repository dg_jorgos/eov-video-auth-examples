import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.commons.codec.binary.Base64;

class Authenticator
{
    /**
     * Create authentication string (security key) for access to video
     *
     * Returned value is **NOT** URL-encoded.
     * This function is suitable for use with URL builders.
     *
     * @param preSharedSecret The secret key. Must match with EOV project's "video_authenticator_secret" setting
     * @param algorithm       Name of algorithm used. Must match EOV project's
     *                        "video_authenticator_hash_algorithm" setting. Supported values: "sha1", "noop".
     * @param uid             UID used in current PURL (personalized URL)
     * @param timestamp       UNIX timestamp
     *
     * @return **NOT** URL-encoded authentication string
     */
    public static String createAuthenticationStringRaw(
            String preSharedSecret,
            String algorithm,
            String uid,
            String timestamp
            ) throws Exception
    {
        String valueToEncode = uid + ":" + timestamp;
        byte[] encoded;
        if (algorithm.equals("noop")) {
            encoded = valueToEncode.getBytes();
        } else if (algorithm.equals("sha1")) {
            encoded = HmacUtils.hmacSha1(preSharedSecret, valueToEncode);
        } else {
            throw new Exception("Unsupported algorithm " + algorithm);
        }
        String encodedB64 = Base64.encodeBase64String(encoded);
        return encodedB64;
    }

    /**
     * Create authentication string (security key) for access to video
     *
     * Returned value is URL-encoded.
     * This function is suitable for direct concatenation of URL pieces together.
     *
     * @param preSharedSecret The secret key. Must match with EOV project's "video_authenticator_secret" setting
     * @param algorithm       Name of algorithm used. Must match EOV project's
     *                        "video_authenticator_hash_algorithm" setting. Supported values: "sha1", "noop".
     * @param uid             UID used in current PURL (personalized URL)
     * @param timestamp       UNIX timestamp
     *
     * @return URL-encoded authentication string
     */
    public static String createAuthenticationString(
            String preSharedSecret,
            String algorithm,
            String uid,
            String timestamp
            ) throws Exception
    {
        String raw = createAuthenticationStringRaw(preSharedSecret, algorithm, uid, timestamp);
        return URLEncoder.encode(raw, StandardCharsets.UTF_8.toString());
    }
}
