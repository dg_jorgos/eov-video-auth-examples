<?php

/**
 * Implementation of auth string (security key) generation for access to EOV project video.
 */
class Authenticator
{
    /**
     * Create authentication string (security key) for access to video
     *
     * @param string $preSharedSecret The secret key. Must match with EOV project's "video_authenticator_secret" setting
     * @param string $algorithm       Name of algorithm used. Must match EOV project's
     *                                "video_authenticator_hash_algorithm" setting. Supported values: "sha1", "noop".
     * @param string $uid             UID used in current PURL (personalized URL)
     * @param string $timestamp       UNIX timestamp
     *
     * @return string URL-encoded authentication string
     */
    public static function createAuthenticationString($preSharedSecret, $algorithm, $uid, $timestamp)
    {
        $keyToHash = $uid . ':' . $timestamp;
        if ($algorithm == 'noop') {
            $hmac = $keyToHash;
        } elseif ($algorithm == 'sha1') {
            $hmac = hash_hmac($algorithm, $keyToHash, $preSharedSecret, true);
        } else {
            throw new Exception("Unsupported algorithm " . $algorithm);
        }
        $b64 = base64_encode($hmac);
        $urlencoded = urlencode($b64);
        return $urlencoded;
    }
}
