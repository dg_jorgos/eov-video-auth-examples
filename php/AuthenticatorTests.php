#!/usr/bin/php
<?php

require_once "Authenticator.php";

$cases = array(
    array(
        'uid'             => 'uid1',
        'timestamp'       => 1490777948,
        'preSharedSecret' => 'preSharedSecret',
        'algorithm'       => 'sha1',
        'expected'        => 'iJSJZ6XycmW8C%2FsJ3koL%2BeXC3FU%3D'
    ),
    array(
        'uid'             => 'uid1',
        'timestamp'       => 1490777948,
        'preSharedSecret' => 'different preSharedSecret',
        'algorithm'       => 'sha1',
        'expected'        => 'c11LOSmJtSd3BggIm9MstFtAj7o%3D'
    ),
    array(
        'uid'             => 'uid2',
        'timestamp'       => 1490777948,
        'preSharedSecret' => 'preSharedSecret',
        'algorithm'       => 'noop',
        'expected'        => 'dWlkMjoxNDkwNzc3OTQ4'
    ),
);

foreach ($cases as $i => $case) {
    echo "Running test $i\n";
    $result = Authenticator::createAuthenticationString(
        $case['preSharedSecret'],
        $case['algorithm'],
        $case['uid'],
        $case['timestamp']
    );
    assert($result === $case['expected']);
}
