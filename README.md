# EngageOne® Video - Embedding the video player on a landing page

This repository provides implementation examples of embedding the EngageOne Video video player into a client-hosted web page.

## Embed Procedure

For security reasons, the EngageOne Video video player is designed to be embedded in a landing page using an iframe. EngageOne Video provides an embeddable link to be used as the source for the iframe. A warning message will be shown instead of playing the video if the video player link is accessed directly rather than through an iframe.

The video player is fluidly responsive by default, expanding to fill the available space. For browsers that do not support this responsive player, default dimensions of 640px wide x 390px high will be used.

> **NOTE: A warning message will be shown instead of playing the video if the video player link is accessed directly rather than through an iframe.**

## Embeddable Links

The full format of the embeddable link is:

```
https://[domain]/[project_name]/videoPlayer.php?uid=[uid]&key=[securityKey]&ts=[timestamp]
```

| Environment                   | Domain
| ----------------------------- | --------------------------------------------------
| **US Pre-Production**         | https://preprod.rtcvid.net
| **US Production**             | https://prod.rtcvid.net
| **EU Production**             | https://euprod.rtcvid.net
| **APAC Pre-Production**       | https://ap-southeast-2-mt-preprod1.rtcvid.net
| **APAC Production**           | https://ap-southeast-2-mt-prod1.rtcvid.net
| **US HIPAA Pre-Production**   | https://us-east-1-mt-preprod2-hipaa.rtcvid.net
| **US HIPAA Production**       | https://us-east-1-mt-prod2-hipaa.rtcvid.net
| **GCP US Pre-Production**     | https://gcp-us-central1-mt-preprod1.rtcvid.net
| **GCP US Production**         | https://gcp-us-central1-mt-prod1.rtcvid.net

For example, for US Pre-Production, the full embedded link would be:

```
https://preprod.rtcvid.net/[project_name]/videoPlayer.php?uid=[uid]&key=[securityKey]&ts=[timestamp]
```

Where:

| URL Parameter                | Description
| ---------------------------- | ----------------
| uid                          | Unique identifier for data lookup. This will determine the content of the video and the data values displayed within this video. This can be encrypted with Pitney Bowes's public key if required.
| key                          | (Optional but strongly recommended.) Time-limited security key. If authentication is enabled for the project then the EngageOne Video platform will only return a video if it receives a correctly generated security key. See **Examples - security key generation** section below.
| ts                           | POSIX time in seconds since 00:00:00 (UTC) Thursday, 1 January 1970. For example `1409141014`.
| eov_source_email_id          | Optional parameter to enable the tracking of emails sent by EngageOne Video. (see below)
| eov_external_source_email_id | Optional parameter to enable the tracking of emails sent by third-party email systems. (see below)

> **Note:** URL Encoding - All query parameters sent in the URL must be URL encoded.

We strongly recommend that all projects use authentication. However, if your project does not require authentication then the video player link can omit the `key` parameter as shown below:

```
https://[domain]/[project_name]/videoPlayer.php?uid=[uid]&ts=[timestamp]
```

## Wrapping the iframe in HTML divs

The responsive video player requires that the video player iframe is wrapped in HTML `div` elements. You can style the `div` elements inline or use a stylesheet.

For ease of embedding, the following HTML fragments can be copied and pasted directly into an HTML page

### HTML fragment with timeline and inline styles

```HTML
<!--
    Keep video player larger than 320px and keep video player within the size of the viewport.
    The value "177.778vh" within the calc() function is the aspect ratio 16/9: 100vh * (16/9).

    54px is the height of the control bar (+ any extra required from the browser).
-->
<div style="min-width: 320px; width: 100%; max-width: calc(177.778vh - 54px); margin: 0 auto;">
    <!-- Maintain aspect ratio of video player. -->
    <div style="position: relative; width: 100%; height: 0px; padding: 56.25% 0px 30px 0px;">
        <iframe
            src="https://[domain]/[project_name]/videoPlayer.php?uid=[uid]&key=[securityKey]&ts=[timestamp]"
            frameborder="0"
            scrolling="no"
            style="position: absolute; top: 0; right: 0; bottom: 0; left: 0; width: 100%; height: 100%; border: none;"
            allowfullscreen>
        </iframe>
    </div>
</div>
```

The HTML fragment above can be wrapped by a further element of any width to set a maximum width for the video player:

```HTML
<div style="max-width: 640px">
```

If the video player is to appear inline with another element or image, ensure it is wrapped by a `<div>` first, for example:

```HTML
<div class="column">
    <!-- PB EOV Fluid Player -->
</div>
<div class="column">
    <!-- Other inline content here -->
    <img />
</div>
```


### HTML fragment without timeline

If the timeline is not required then use the following HTML fragment:

```HTML
<!--
    Keep video player larger than 320px and keep video player within the size of the viewport.
    The value "177.778vh" within the calc() function is the aspect ratio 16/9: 100vh * (16/9).
-->
<div style="min-width: 320px; width: 100%; max-width: calc(177.778vh); margin: 0 auto;">
    <!-- Maintain aspect ratio of video player. -->
    <div style="position: relative; width: 100%; height: 0px; padding: 56.25% 0px 0px 0px;">
        <iframe
            src="https://[domain]/[project_name]/videoPlayer.php?uid=[uid]&key=[securityKey]&ts=[timestamp]"
            frameborder="0"
            scrolling="no"
            style="position: absolute; top: 0; right: 0; bottom: 0; left: 0; width: 100%; height: 100%; border: none;"
            allowfullscreen>
        </iframe>
    </div>
</div>
```

### HTML fragment with style sheet

An alternative to using inline styles is to specify CSS rules in a separate style sheet. This has the advantage that you can use media queries to improve the responsive layout of the complete page.

**CSS Stylesheet:**

```CSS
#EngageOne-Video-vidbill-wrapper {
    min-width: 320px;
    width: 100%;
    margin: 0 auto;
    max-width: calc(177.778vh - 54px);
}

#EngageOne-Video-vidbill-wrapper.no-timeline {
    max-width: calc(177.778vh);
}

#EngageOne-Video-vidbill-container {
    position: relative;
    width: 100%;
    height: 0px;
    padding: 56.25% 0px 30px 0px;
}

.no-timeline #EngageOne-Video-vidbill-container {
    padding: 56.25% 0px 0px 0px;
}

#EngageOne-Video-vidbill-container iframe {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    border: none;
}

/**
 * This is the theoretical breakpoint as to which the video is no longer adjacent to other elements of the page.
 */
@media screen and (max-width: 640px) {
    #EngageOne-Video-vidbill-wrapper {
        max-width: calc(177.778vh - 54px);
        /**
         * 177.778vh is the aspect ratio 16/9: 100vh * (16/9).
         * 54px is the height of the timeline bar (+ any extra required from the browser).
         */
    }
}
```

**HTML Fragment:**

```HTML
<!-- To display it without timeline bar add the class "no-timeline" as follows: -->
<!-- <div id="EngageOne-Video-vidbill-wrapper" class="no-timeline"> -->

<div id="EngageOne-Video-vidbill-wrapper">
    <div id="EngageOne-Video-vidbill-container">
        <iframe
            src="https://[domain]/[project_name]/videoPlayer.php?uid=[uid]&key=[securityKey]&ts=[timestamp]"
            frameborder="0"
            scrolling="no"
            style="position: absolute; top: 0; right: 0; bottom: 0; left: 0; width: 100%; height: 100%; border: none;"
            allowfullscreen>
        </iframe>
    </div>
</div>
```

## Email Tracking

When the video player is embedded in a client hosted landing page, you must append an URL parameter to the embeddable video player link that indicates the source of the email. This ensures that visits from URLs with an email source URL parameter are included in the email reports. There are separate URL parameters for emails sent by EngageOne Video and by third-party systems.

### Tracking emails sent by EngageOne Video

If your project uses EngageOne Video to send emails it is recommended to include this parameter in your video player links. The value of `eov_source_email_id` in the request to the page embedding the video should be used as the value in the video player link, e.g.

```
https://[domain]/[project_name]/videoPlayer.php?uid=[uid]&key=[securityKey]&ts=[timestamp]&eov_source_email_id=[unique_id]
```

> **NOTE:** if this parameter is not included in the video player link it will not be possible to track the source of emails sent from the EngageOne Video platform.

### Emails sent by third-party systems

If your project uses the `eov_external_source_email_id` URL parameter to associate visits to EngageOne Video with emails generated by a third-party system you **must** include this parameter in the video player link, e.g.

```
https://[domain]/[project_name]/videoPlayer.php?uid=[uid]&key=[securityKey]&ts=[timestamp]&eov_external_source_email_id=[unique_id]
```

> **NOTE:** it is possible to change the name of this parameter using a project setting. If you do this, you will need to update the video player link to match.

## Fullscreen mode on iOS 10 devices

All Apple devices running iOS 10 can display the video in a non-native fullscreen mode that behaves more like the desktop experience.
However, this functionality may be disabled or unreliable in the following scenarios, and extensive device and browser testing is recommended:

* The page domain is different to the iframe domain
* The iframe is embedded in another iframe

More information can be found in the [Developer Guide](https://admin.rtcvid.net/admin/docs/eovHelp.htm#cshid=1).

## Generating the security key for the video player link

If authentication is enabled for the project then the EngageOne Video platform will only return a video if it receives a correctly generated security key.

It will read the uid and ts (timestamp) fields sent in the request, re-build the signature from the data in these fields, and check that the results match the generated security key.

If it cannot verify the key, or the timestamp is more than 60 minutes before the request is received by the server, then the HTTP response will contain `401 Authorization Required` and a video will not be shown. The time period is configurable (see below).

You need to write the server-side code that will generate the security key.

1. Request the pre-shared secret for your project:
    * On the management portal, go to **Administration > Embedded Video Player**.
    * Click the Get **Pre-shared Secret** button.
1. Your code should do the following:
    * Encrypt the string `[uid]:[timestamp]` with the supplied pre-shared secret.
        * By default EngageOne Video uses HMAC/SHA1 for encryption. You can use SHA512 provided that you configure the project to support this (see below for details).
    * Base64 encode the resulting encrypted string.
    * URL encode the Base64-encoded string.

## Examples - security key generation

This repository contains a number of directories with examples of how to generate the security key in different programming languages.

* `/js` - Javascript implementation
* `/php` - PHP implementation
* `/java` - Java implementation
* `/dotnet` - C#/.NET implementation

In each implementation example there is a file called `Authenticator` which contains the source code for generating the securityKey value, and a file called `AuthenticatorTests` which contains tests to ensure the implementation works correctly.

## Configuring authentication of video requests

You can configure some aspects of the authentication process. By default:

* Authentication for a video project is enabled.
* The encryption method is sha1.
* The authentication token used in a video request is valid for 60 minutes.

To disable authentication for a project, set the `video_authenticator_enabled` project setting to `false`.

To change the encryption method, set `video_authenticator_hash_algorithm` to `sha512`.

To change the valid time period for the authentication token, set `video_authenticator_expiry_seconds` to the required time period in seconds.

Additionally, the pre-shared secret can be set by using the `video_authenticator_secret` project setting and setting it to the string value you want.

## License

This repository is made public for easy use by developers working on EngageOne Video projects. It is allowed to modify and redistribute this code.

The contents of this repository are published under **the MIT License**.

See `LICENSE` file in this repository for full text of license.
